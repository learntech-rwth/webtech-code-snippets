<?php
require_once(__DIR__.'/../src/Application.php');
require_once(__DIR__.'/../src/NavigationItem.php');
require_once(__DIR__.'/../src/Page/Seite1.php');
require_once(__DIR__.'/../src/Page/Seite2.php');

$app = new Application( );

$p1 = new \Page\Seite1( );
$nav1 = new NavigationItem( 'Gruss', 'p1');

$p2 = new \Page\Seite2( );
$nav2 = new NavigationItem( 'Die 2.Seite', 'p2');

$app->addPage( 'p1', $p1 );
$app->addNavigationItem( $nav1 );
$app->addPage( 'p2', $p2 );
$app->addNavigationItem( $nav2 );

$app->run( );

?>
