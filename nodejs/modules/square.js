let area = function(k) {
    return k*k;
}

let perimeter = function (k) {
    return 4*k;
}

module.exports = {
    area,
    perimeter
}