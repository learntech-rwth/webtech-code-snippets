const sq = require('./square.js')

console.log("Flächeninhalt des Quadrats (4cm Kanten) ist: "+sq.area(4)+"cm^2");
console.log("Umfang des Quadrats (4cm Kanten) ist: "+sq.perimeter(4)+"cm");