// save all clients with a Map
// map knows which client subscribes to which chat
let chatSubscriptions = new Map();

// handle incoming messages with a custom EventEmitter
const EventEmitter = require('events');
class ChatServer extends EventEmitter {}
const chat = new ChatServer();

// handle chat events (minimal proprietary chat protocal)

// handle incoming messages -> broadcast them
chat.on('message', (ws, message) => {
  let arr = chatSubscriptions.get(message.chatID) || [];
  broadcastToArr(arr, ws, message);
});
chat.on('subscribe', (ws, message) => {
  // client has subscribed an event -> put him in chatSubscriptions
  let arr = chatSubscriptions.get(message.chatID) || [];
  addToArray(arr, ws);  // uses array.push(...); doesn't change the array in map by reference; update by hand required!
  chatSubscriptions.set(message.chatID, arr);
});
// client has unsubscribed an event -> delete him from chatSubscriptions
chat.on('unsubscribe', (ws, message) => {
  let arr = chatSubscriptions.get(message.chatID) || [];
  deleteFromArray(arr, ws); // uses array.splice(...); changes the array in map by reference; no update needed by hand required!
});


// WebSocketServer Lib
const WebSocketServer = require('ws').Server;

// export a function which adds our ws server to an HTTP server
module.exports = function createChatServer(httpServer) {
  // create WS server instanz
  let wss = new WebSocketServer({ server: httpServer });

  // add functions to the events of the WS server instanz
  wss.on('connection', function connection(ws) {

    // on incoming messages: print them und let our chatEmitter handle them
    ws.on('message', (message) => {
      console.log('received: %s', message);
      message = JSON.parse(message);
      chat.emit(message.type, ws, message);
    });

    // on close: unsubscribe User from all subscriptions
    ws.on('close', () => {
      chatSubscriptions.forEach( (arr, key, map ) => {
        chat.emit("unsubscribe", ws, {chatID: key} )
      });
      console.log(' User disconnected');
    });
  });
}

//HELPER FUNCTION
function broadcastToArr(arr, ws,message) {
  message = JSON.stringify(message);
  for(var i = 0; i< arr.length; i++) {
    if(arr[i] != ws) {
      arr[i].send(message);
    }
  }
}
function deleteFromArray(arr, ws) {
  for(var i = 0; i< arr.length; i++) {
    if(arr[i] == ws) {
      arr.splice(i,1);
      break;
    }
  }
}
function addToArray(arr, ws) {
  arr.push(ws);
}
