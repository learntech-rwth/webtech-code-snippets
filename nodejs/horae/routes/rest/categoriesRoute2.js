let express = require('express');
let router = express.Router();

let DBManager = require('../../services/DBManager');
let config = require('../../config.json');


// GET list of all events
router.get('/', function(req, res, next) {
  DBManager.getConnection(config.dbURL, (db) => {
    let categorieCollection = db.collection('categories');

    categorieCollection.find().toArray( (err, array) => {
      if (err) throw err;
      res.json(array);
    });
  });
});

module.exports = router;
