const express = require('express');

const app = express();

app.get('/', function(req, res) { 
    res.send('Hello World!');
});

app.use(function(err, req, res, next){
    console.error(err.stack)
    res.status(500).send('Error found')
})

app.listen(8080, console.log('App listening on port 8080!'));
