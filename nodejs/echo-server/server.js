const net = require('net')

let server = net.createServer(function(socket) {
    socket.write('Server response\n')
    socket.on('data',function(chunk){
        socket.write(chunk)
    });
    socket.on('end', socket.end);
});

server.listen(1320,'127.0.0.1');
console.log("TCP-Server running at 127.0.0.1:1320");