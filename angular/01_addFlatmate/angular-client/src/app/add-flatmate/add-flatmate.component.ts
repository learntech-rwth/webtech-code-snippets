import { Component, OnInit } from '@angular/core';
import { Flatmate } from '../flatmate';

@Component({
  selector: 'app-add-flatmate',
  templateUrl: './add-flatmate.component.html',
  styleUrls: ['./add-flatmate.component.scss']
})
export class AddFlatmateComponent implements OnInit {

  userCreated: boolean = false;

  newFlatmate: Flatmate = new Flatmate(-1, '');

  addFlatmate(){

  }

  constructor() { }

  ngOnInit(): void {
  }

}
