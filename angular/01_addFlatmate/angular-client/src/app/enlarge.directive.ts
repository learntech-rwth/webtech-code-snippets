import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appEnlarge]'
})
export class EnlargeDirective {

  @Input('appEnlarge') zoomValue: string = '200';

  constructor(private element: ElementRef) { }

  @HostListener('mouseenter') onMouseEnter(){
    this.enlarge(+this.zoomValue);
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.enlarge(100);
  }

  private enlarge(value: number): void {
    this.element.nativeElement.style.fontSize = value + '%';
  }

}
