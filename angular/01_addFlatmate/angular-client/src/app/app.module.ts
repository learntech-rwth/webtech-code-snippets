import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnlargeDirective } from './enlarge.directive';
import { TempInfoDirective } from './temp-info.directive';
import { AddFlatmateComponent } from './add-flatmate/add-flatmate.component';

@NgModule({
  declarations: [
    AppComponent,
    EnlargeDirective,
    TempInfoDirective,
    AddFlatmateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
