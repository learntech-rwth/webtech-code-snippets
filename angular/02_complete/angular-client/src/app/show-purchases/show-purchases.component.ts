import { Component, OnInit } from '@angular/core';
import { Purchase } from '../purchase';
import { HttpCommunicatorService } from '../http-communicator.service';

@Component({
  selector: 'app-show-purchases',
  templateUrl: './show-purchases.component.html',
  styleUrls: ['./show-purchases.component.scss']
})
export class ShowPurchasesComponent implements OnInit {

    purchaseRemoved: boolean = false;
    purchaseNotRemoved: boolean = false;

    purchases: Purchase[] = [];

    constructor(private http: HttpCommunicatorService) {
      this.getAllPurchases();
    }

    getAllPurchases(): void {
      this.http.getAllPurchases().subscribe(resp => {
        if (resp.status === 200 && resp.body !== null) {
          this.purchases = resp.body;
        }
      });
    }

    deletePurchase(id: number): void {
      this.http.deletePurchase(id).subscribe(resp => {
        if (resp.status === 200) {
          this.purchaseRemoved = true;
          this.purchaseNotRemoved = false;
          this.getAllPurchases();
        }
        else {
          this.purchaseRemoved = false;
          this.purchaseNotRemoved = true;
        }
      });
    }

  ngOnInit() {
  }

}
