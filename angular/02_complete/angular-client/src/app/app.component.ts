import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title: string = 'angular-client';

  public constructor(private router: Router, private titleService: Title){

  }

  setPageTitle(): void{
    let currentTitle: string = 'WG Finanzen';
    switch(this.router.url){
      case '/': currentTitle = currentTitle + ' - Neuer Mitbewohner';
        break;
      case '/addPurchase': currentTitle = currentTitle + ' - Neuer Kauf';
        break;
      case '/showPurchases': currentTitle = currentTitle + ' - Käufe';
        break;
      case '/balances': currentTitle = currentTitle + ' - Bilanzen';
        break;
    }
    this.titleService.setTitle(currentTitle);
  }

  isActive(path: string): string{
    if(path === '/'){
      this.setPageTitle();
    }
    return (path === this.router.url) ? 'active' : '';
  }
}
