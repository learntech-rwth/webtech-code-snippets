export class Purchase {
    id: number = -1;
    title: string;
    description: string;
    cost: number;
    date: string;
    boughtBy: string;
    boughtFor: string[];

    constructor(title: string, description: string, cost: number, date: string, boughtBy: string, boughtFor: string[]){
        this.title = title;
        this.description = description;
        this.cost = cost;
        this.date = date;
        this.boughtBy = boughtBy;
        this.boughtFor = boughtFor;
    }
}