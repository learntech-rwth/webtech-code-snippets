import { Component, OnInit } from '@angular/core';
import { Purchase } from '../purchase';
import { Flatmate } from '../flatmate';
import { HttpCommunicatorService } from '../http-communicator.service';

@Component({
  selector: 'app-balances',
  templateUrl: './balances.component.html',
  styleUrls: ['./balances.component.scss']
})
export class BalancesComponent implements OnInit {

    flatmates: Flatmate[] = [];
    purchases: Purchase[] = [];

    rows: any[] = [];
    whoToWho: any = {};

    constructor(private http: HttpCommunicatorService) {
      this.http.getAllFlatmates().subscribe(resp => {
        if (resp.status === 200 && resp.body !== null) {
          this.flatmates = resp.body;
          this.setupWhoToWho();
          this.getAllPurchases();
        }
      });
    }

    getAllPurchases(): void {
      this.http.getAllPurchases().subscribe(resp => {
        if (resp.status === 200 && resp.body !== null) {
          this.purchases = resp.body;
          this.computeBalances();
        }
      });
    }

    getIDByName(name: string): number {
        let id = -1;
        this.flatmates.forEach(function(flatmate){
            if(flatmate.name === name){
                id = flatmate.id;
            }
        });
        return id;
    }

    setupWhoToWho(): void {
        this.flatmates.forEach((flatmate: Flatmate) => {
            this.whoToWho[flatmate.name] = {};
        });
        let keys = Object.keys(this.whoToWho);
        let i: number;
        for(i = 0; i < keys.length; i = i + 1){
            this.flatmates.forEach((flatmate) => {
              if (keys[i] != flatmate.name) {
                this.whoToWho[keys[i]][flatmate.name] = 0;
              }
            });
        }
    }

    computeBalances(): void {
        this.purchases.forEach((purchase) => {
            let dif = purchase.cost/purchase.boughtFor.length;
            purchase.boughtFor.forEach((flatmate) => {
                if(flatmate != purchase.boughtBy){
                    this.whoToWho[flatmate][purchase.boughtBy] = this.whoToWho[flatmate][purchase.boughtBy] + dif;
                }
            });
        });
        this.prepareBalances();
    }

    prepareBalances(): void {
        let mainKeys = Object.keys(this.whoToWho);
        let i, j;
        for(i = 0; i < mainKeys.length; i = i + 1){
            let subkeys = Object.keys(this.whoToWho[mainKeys[i]]);
            for(j = 0; j < subkeys.length; j = j + 1){
                if(this.whoToWho[mainKeys[i]][subkeys[j]] > 0){
                    this.rows.push({
                        name: mainKeys[i],
                        to: subkeys[j],
                        amount: this.whoToWho[mainKeys[i]][subkeys[j]]
                    });
                }
            }
        }
    }

  ngOnInit() {
  }

}
