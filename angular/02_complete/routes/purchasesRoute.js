var router = require('express').Router();

var Supporter = require('../modules/support.js');

var sup = new Supporter();

var purchases = [
    {
        id: 0,
        date: '25.07.2018',
        title: 'Kartoffeln',
        cost: 199,
        boughtBy: 'Bob',
        description: '2 kg Kartoffeln',
        boughtFor: ['Alice', 'Bob', 'Eve']
    },
    {
        id: 1,
        date: '25.07.2018',
        title: 'Bäcker',
        cost: 430,
        boughtBy: 'Alice',
        description: '5 Brötchen, 3 Croissants',
        boughtFor: ['Alice', 'Bob', 'Eve']
    },
    {
        id: 2,
        date: '25.07.2018',
        title: 'Supermarkt',
        cost: 219,
        boughtBy: 'Alice',
        description: 'Milch, Joghurt',
        boughtFor: ['Alice']
    },
    {
        id: 3,
        date: '25.07.2018',
        title: 'Markt',
        cost: 542,
        boughtBy: 'Alice',
        description: 'Zucchini, Paprika, Zwiebeln',
        boughtFor: ['Alice', 'Bob', 'Eve']
    },
    {
        id: 4,
        date: '25.07.2018',
        title: 'Supermarkt',
        cost: 149,
        boughtBy: 'Bob',
        description: 'Getränk',
        boughtFor: ['Eve']
    }
];

router.get('/', function(req, res) {
    res.json(purchases);
});

router.get('/:id', function(req, res){
    if(purchases[req.params.id] === undefined){
        res.send('No such purchase available');
    }
    else{
        res.send(purchases[req.params.id]);
    }
});
//*
router.post('/', function(req, res){
    if(sup.isUndefinedOrEmpty(req.body.date)){
        res.status(404).send('date required');
    }
    else{
        var newPurchase = {
            id: purchases.length,
            date: req.body.date,
            title: req.body.title,
            cost: req.body.cost,
            boughtBy: req.body.boughtBy,
            description: req.body.description,
            boughtFor: req.body.boughtFor
        };
        var dateParts = newPurchase.date.split('-');
        newPurchase.date = dateParts[2].substring(0,2) + '.' + dateParts[1] + '.' + dateParts[0];
        purchases.push(newPurchase);
        res.send(purchases[purchases.length-1]);
    }
});//*/
/*
router.put('/:id', function(req, res){

});*/
//*
router.delete('/:id', function(req, res){
    var purchase = sup.findID(purchases, parseInt(req.params.id))
    if(purchase === undefined){
        res.send('No such purchase available');
    }
    else{
        purchases.splice(purchases.indexOf(purchase), 1);
        res.json(purchase);
    }
});//*/

module.exports = router;