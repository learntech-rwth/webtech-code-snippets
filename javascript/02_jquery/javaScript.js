
/* Aufgerufen, wenn Button Hinzufügen ausgelöst wird.
   Holt sich die Eingaben aus dem Formular und schreibt diese in eine Tabelle in
   das aktuelle Dokument.
*/
function addVL( ){

	var name = document.getElementById("name").value;
					// Formularfeld für Titel der Vorlesung
	var inhalt = document.getElementById("inhalt").value; // Textarea Inhalt
	var datum = document.getElementById("datum").value; // input date
	var pw = document.getElementById("pw").value; // Password-field
	var rObj = document.getElementsByName("VLÜ"); // Radio Button
	var vlü = rObj[0].checked ? rObj[0].value : rObj[1].value;

	var row = '<td>'+name+'</td>'
		+'<td>'+inhalt+'</td>'
		+'<td>'+datum+'</td>'
		+'<td><button onclick="deleteVL(this); return false;">Delete</button></td>';

	// alert( row );

	var insertIn;
	if (vlü == "Vorlesung"){
		insertIn = document.getElementById("VL"); // Vorlesungstabelle adressieren
		pwVL.push(pw); // Password merken, aktueller Index inkrementiert
		vüIndex= 'V'+pwVL.length; // Id für erzeugte Vorlesungs-Tabellenzeile
	} else {
		insertIn = document.getElementById("Ü"); // VÜbungstabelle adressieren
		pwÜ.push(pw);
		vüIndex= 'Ü'+pwÜ.length;
	}


	var neueZeile = document.createElement('tr'); // neue Tabellenzeite erzeugen
	neueZeile.id=vüIndex; // Id dieser Zeile setzen
	neueZeile.innerHTML = row; // Inhalt der Tabellenzeile setzen (s.o.)
	insertIn.appendChild( neueZeile ); // an die adressierte Tabelle neue Zeile Anhängen

}

var pwVL = [];  // Passworte zum Löschen der Vorlesungen, gleichzeitig Index für Id
var pwÜ = [];   //dto.für Übungen
var rowId;      // globale Variable für Seiteneffekte
var row;        // globale Variable für Seiteneffekte
var list;       // globale Variable für Seiteneffekte
var vüIndex;    // Indexim pw-Array und zur Generierung von IDs


/* Löschen eines Tabelleneintrags vorbereiten => Dialog für Password
*/
function deleteVL( element ){
	rowId = element.parentNode.parentNode.id; // Großvater des geklickten Button => Zeile
	row = rowId.slice( 1 ); // V bzw. Ü aus der Id ausblenden
	list =  element.parentNode.parentNode.parentNode.id; // Urgroßvater => die Tabelle
	document.getElementsByTagName("dialog")[ 0 ].show( ); // Anzeigen des Password-Dialogs
}


/* Löschen der identifizierten Tabellenzeile nach password-OK ausführen */
function deleteRow( ){
	document.getElementsByTagName("dialog")[ 0 ].close( ); // (die einzige existierende) Dialogbox wieder schließen
	if(list == "VL"){ // in der Vorlesungstabelle
		if( pwVL[row-1] == document.getElementById("pwCheck").value ) {
			document.getElementById("VL").removeChild(document.getElementById(rowId)); // über Id adressierte Zeile aus der Tabelle löschen
			//	in pwVL wird das Password nicht gelöscht, damit der Index (für Id) bestehen bleibt
		}else{
			alert("Falsches Password! Löschen Abgebrochen!");
		}
	} else{
		if(pwÜ[row-1] == document.getElementById("pwCheck").value){
			document.getElementById("Ü").removeChild(document.getElementById(rowId));
		}else{
			alert("Falsches Password! Löschen Abgebrochen!");
		}
	}
}


/* registriert beim Hinzufüge- und Löschbuttons die jeweils aufzurufende Callback */
function registerCallbacks( ) {
	var addButton = document.getElementById('add');
	addButton.addEventListener('click', function(evt){ addVL(); evt.preventDefault();});
	// evt.preventDefault verhindert, dass die Seite nach Ausführung der Callback neu geladen wird
	var deleteButton = document.getElementById('cancel'); // in der Dialogbox
	deleteButton.addEventListener('click', function(evt){ deleteRow(); evt.preventDefault();});
}

/* registerCallbacks wird beim Event Dokumentgeladen aufgerufen */
document.addEventListener( 'DOMContentLoaded', registerCallbacks );